var React = require('react');
var ReactDOM = require('react-dom');
var InputText = require('./input-text');
var InputSelect = require('./input-select');
var InputDate = require('./input-date-range');

module.exports = React.createClass({
  render: function() {

    if (this.props.fieldType == 'text') {
      return <span>
        <InputText {...this.props} key={this.props.id}/>
    </span>
    } else if (this.props.fieldType == 'select') {
      return <span>
      <InputSelect {...this.props} key={this.props.id}/>
    </span>
    } else if (this.props.fieldType == 'date') {
  return <span>
      <InputDate {...this.props} key={this.props.id}/>
    </span>
}
  },

  handleInputTextChange: function(event) {
    console.log('Input changed');
  }
});
