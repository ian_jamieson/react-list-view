var React = require('react');
var ReactDOM = require('react-dom');
var Select = require('react-select');

module.exports = React.createClass({
    componentWillMount: function() {
        return {
            //options: this.props.options,
        }
    },

    render: function() {
        var options = [
            { value: 'one', label: 'One' },
            { value: 'two', label: 'Two' }
        ];
            return <span>
                <Select
                    name={this.props.fieldName}
                    options={options}
                    //searchable='true'
                    placeholder={this.props.fieldPlaceholder}
                    onChange={this.handleSelectChange}
                />

    </span>
    },

    handleSelectChange: function(event) {
        console.log('Select changed');
    }
});