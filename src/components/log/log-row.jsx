var React = require('react')

module.exports = React.createClass({
  render: function() {
    return <li>
    {this.props.name} {this.props.action} {this.props.body}
    </li>
  }
});
