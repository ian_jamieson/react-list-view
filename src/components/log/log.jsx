var React = require('react')
var LogRow = require('./log-row');

module.exports = React.createClass({
  render: function() {

    var list = this.props.data.map(function(listData){
            return <LogRow {...listData} />
    });

    return <div className="widget log">
      <h3>{this.props.title}</h3>
      <ul>{list}</ul>
    </div>

}
});
