var React = require('react');
var ReactDOM = require('react-dom');
var ListItem = require('./list-item');

module.exports = React.createClass({

    getInitialState: function() {
        return {
            checked:  false
        }
    },


    __toggleSelected: function(event) {
        this.setState({
            checked: this.refs.checked.checked
        });
        console.log(this.refs.checked.checked);
        return
    },


  render: function() {

      //debugger
      var listData = this.props.listData;
      var ListItems = this.props.fields.map(function(field, index) {
                  return <ListItem value={listData[field.fieldKey]} field={field} key={'field-' + index}/>;
          });

    return <tr onDoubleClick={this.__editRow}>
        {ListItems}
        <td className="text-center">
            <span className="btn"><input type="checkbox" value="1" ref="checked" checked={this.state.checked || this.props.checked} onChange={this.__toggleSelected}/></span>
            <a className="btn" onClick={this.__editRow} title="Modify this Item"><i className="md md-edit"></i></a>
            <a className="btn" onClick={this.__deleteRow} title="Delete this Item"><i className="md md-close"></i></a>
        </td>
    </tr>
  },

    __selectRow: function(event) {
        console.log('Select row');
    },

    __editRow: function(event) {
       console.log('Edit row');
    },

    __deleteRow: function(event) {
        console.log('Delete row');
    }



});
