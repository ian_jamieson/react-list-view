var React = require('react');
var ReactDOM = require('react-dom');
var Input = require('./../input/input');

module.exports = React.createClass({
  render: function() {
    return <td>
      <span className={'input ' + this.props.fieldType}><Input {...this.props} /></span>
      <ul className="options">
        <li><a className="btn" onClick={this.handleReOrderUp} title="Filter Ascending"><i className="md md-expand-more"></i></a></li>
        <li><a className="btn" onClick={this.handleReOrderDown}  title="Filter Descending"><i className="md md-expand-less"></i></a></li>
      </ul>
    </td>
  },

  handleReOrderUp: function(event) {
    console.log('ReOrderUp');
  },
  handleReOrderDown: function(event) {
    console.log('ReOrderDown');
  },
});
