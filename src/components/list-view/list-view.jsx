var React = require('react');
var ReactDOM = require('react-dom');
var ListHeader = require('./list-header');
var ListRow = require('./list-row');



module.exports = React.createClass({
    getInitialState: function() {
        return {
            //checked: ReactDOM.findDOMNode(this.refs.checked)
        };
    },

    componentDidMount: function() {
        console.log('Children ' + this.props.children);
        //debugger
    },

    hasValidFieldKeys: function(fields, listData) {
        var invalidFields = fields.filter(function(field) {
            var hasKey = field.fieldKey in listData;

            if(!hasKey) {
                console.error(field.fieldKey + " doesn't exist. Please make sure you match your 'fieldKey' to an existing column in your data table");
            } else {
                //console.info(field.fieldKey + ': ' + listData[field.fieldKey] + ' type:' + field.fieldType);
            }

            return !hasKey;
        });

        return invalidFields.length === 0;
    },



    render: function() {

        var self = this;

      var data = this.props.data;
      var fields = this.props.fields;
      var tableSettings = this.props.settings;

    var header = fields.map(function(headerData){

        return <ListHeader {...headerData} data={fields} key={headerData.id}/>
          });


        var list = this.props.data.filter(function(listData) {
            return self.hasValidFieldKeys(self.props.fields, listData);
        }).map(function(listData){
            return <ListRow listData={listData} fields={fields} key={listData.id}/>
          });

      var totalResults = 1572;
      var countFields = header.length;
      var columnCount = countFields + 1; // +1 for table Actions

          return <table className="list-view table table-bordered table-striped table-hover">
            <thead className="head">
              <tr>
                <th colSpan={columnCount}>{this.props.settings.title} <span className="text-muted pull-right">{totalResults} results</span></th>
              </tr>
            </thead>
              <tbody className="body">
            <tr>{header}
                <td className="text-center">
                    <a className="btn" onClick={this.__filter} title="Filter List"><i className="md md-filter-list"></i></a>
                    <a className="btn" onClick={this.__showMoreFilters} title="Delete All Selected"><i className="md md-add"></i></a>
                    <a className="btn" onClick={this.__toggleSelectAll} title="Select All"><i className="md md-select-all"></i></a>
                    <a className="btn" onClick={this.__deleteSelected} title="Delete All Selected"><i className="md md-close"></i></a>
                </td>
            </tr>
            {list}
              </tbody>
              <tfoot className="footer">
                  <tr>
                    <td colSpan={columnCount}>
                        <button className="btn btn-prev">Previous</button>
                        <button className="btn btn-next">Next</button>
                    </td>
                  </tr>
              </tfoot>
          </table>
},
    __filter: function(event) {
        console.log('Filter results...');
    },
    __showMoreFilters: function(event) {
        console.log('Show more filters...');
    },
    __toggleSelectAll: function(event) {
        /*React.Children.forEach(this.props.checkbox, (child) => {
            console.log('Child: ' + ReactDOM.findDOMNode(this.refs.checked));
        });*/
        var checkbox = ReactDOM.findDOMNode(this.refs.checked);
        var children = this.props.children;

        this.setState({
            checked: true
        });



        console.log('Select All');

    },
    __deleteSelected: function(event) {
        for(var key in this.state.checked) {
            if (this.state.data[key].checked === true) {

            }
        }
        console.log('Delete Selected');
    },
});
