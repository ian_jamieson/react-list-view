var React = require('react');
var ReactDOM = require('react-dom');
//var Api = require('./utils/api');
var ListView = require('./components/list-view/list-view');
var UserLog = require('./components/log/log');

var listData = {
  data: [
    {
    id: '1',
    name: 'test name',
    calling_gt: '1234',
    op_code: '9876',
      sender_id: '1233'
  },
  {
    id: '2',
    name: 'test 2',
    calling_gt: '678',
    op_code: '563454',
    sender_id: '564563',
  },
  {
    id: '3',
    name: 'test 3',
    calling_gt: '45435',
    op_code: '67675',
    sender_id: '6435353'
  },
    {
      id: '4',
      name: 'test 4',
      calling_gt: '45435',
      op_code: '67675',
      sender_id: '6435353'
    },
    {
      id: '5',
      name: 'test 5',
      calling_gt: '45435',
      op_code: '67675',
      sender_id: '6435353'
    },
    {
      id: '6',
      name: 'test 6',
      calling_gt: '45435',
      op_code: '67675',
      sender_id: '6435353'
    },
    {
      id: '7',
      name: 'test 7',
      calling_gt: '45435',
      op_code: '67675',
      sender_id: '6435353'
    },
    {
      id: '8',
      name: 'test 8',
      calling_gt: '45435',
      op_code: '67675',
      sender_id: '6435353'
    }
],
fields: [
  {
    id: '1',
    fieldName: 'Name',
    fieldType: 'select',
    fieldSearchable: true,
    fieldSearchType: 'contains',
    fieldMultiple: false,
    fieldEditable: false,
    fieldPlaceholder: 'Name',
    fieldFormPosition: '1',
    fieldListPosition: '1',
    fieldListShow: true,
    fieldListSubSearch: false,
    fieldDataSource: 'messages',
    fieldKey: 'name',
    fieldValue: 'id',
    createdAt: '2015-11-30T11:02:34.000Z',
    updatedAt: '2015-11-30T11:02:34.000Z'
  },
  {
    id: '2',
    fieldName: 'Calling GT',
    fieldType: 'select',
    fieldSearchable: true,
    fieldSearchType: 'contains',
    fieldMultiple: false,
    fieldEditable: false,
    fieldPlaceholder: 'Calling GT',
    fieldFormPosition: '1',
    fieldListPosition: '1',
    fieldListShow: true,
    fieldListSubSearch: false,
    fieldDataSource: 'messages',
    fieldKey: 'calling_gt',
    fieldValue: 'id',
    createdAt: '2015-11-30T11:02:34.000Z',
    updatedAt: '2015-11-30T11:02:34.000Z'
  },
  {
    id: '3',
    fieldName: 'OP Code',
    fieldType: 'select',
    fieldMultiple: false,
    fieldEditable: false,
    fieldPlaceholder: 'OP code',
    fieldFormPosition: '1',
    fieldListPosition: '1',
    fieldSearchable: true,
    fieldSearchType: 'exact',
    fieldListShow: true,
    fieldListSubSearch: false,
    fieldDataSource: 'messages',
    fieldKey: 'op_code',
    fieldValue: 'id',
    createdAt: '2015-11-30T11:02:34.000Z',
    updatedAt: '2015-11-30T11:02:34.000Z'
  },
  {
    id: '4',
    fieldName: 'Sender ID',
    fieldType: 'text',
    fieldMultiple: false,
    fieldEditable: false,
    fieldPlaceholder: 'Sender ID',
    fieldFormPosition: '1',
    fieldListPosition: '1',
    fieldSearchable: true,
    fieldSearchType: 'startsWith',
    fieldListShow: true,
    fieldListSubSearch: false,
    fieldDataSource: 'messages',
    fieldKey: 'sender_id',
    fieldValue: 'id',
    createdAt: '2015-11-30T11:02:34.000Z',
    updatedAt: '2015-11-30T11:02:34.000Z'
  },
 /* {
    id: '5',
    fieldName: 'Made up',
    fieldType: 'date',
    fieldMultiple: false,
    fieldEditable: false,
    fieldPlaceholder: 'Made Up',
    fieldFormPosition: '1',
    fieldListPosition: '1',
    fieldListShow: true,
    fieldListSubSearch: false,
    fieldSearchable: true,
    fieldSearchType: 'contains',
    fieldDataSource: 'messages',
    fieldKey: 'made_up',
    fieldValue: 'id',
    createdAt: '2015-11-30T11:02:34.000Z',
    updatedAt: '2015-11-30T11:02:34.000Z'
  }*/
],
settings:
  {
    title: 'Quarantine Vault',
    dataSource: 'message',
    allowDelete: true,
    permanentlyDelete: true,
    markedDeletedFieldName: 'willDelete',
    allowEdit: true,
    saveToTable: 'quarantine',
    defaultLimit: 15,
    defaultOrder: 'createdAt asc'
  }

};

var logData = {
  title: 'User Log',
  data: [
    {
    id: '1',
    name: 'Ian',
    action: 'Updated',
    body: 'hardblockgt'
  },
  {
    id: '1',
    name: 'Mark',
    action: 'Added',
    body: 'hardblocksid'
  },
  {
    id: '1',
    name: 'Marek',
    action: 'Updated',
    body: 'hardblockgt'
  }
  ]
};


//var side = React.createElement(UserLog, logData);
//React.render(side, document.querySelector('aside.side'));

var main = React.createElement(ListView, listData);
ReactDOM.render(main, document.querySelector('main.quarantine'));
