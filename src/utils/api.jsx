var Fetch = require('whatwg-fetch');
var rootUrl = 'http://localhost:1337/api/v2/';
var apiKey = '430d6820d865788';

module.exports = {
  get: function(url) {
    return fetch(rootUrl + url, {
      headers: {
        'Authorization': 'Client-ID ' + apiKey
      }
    })
    .then(function(response){
      return response.json()
    })
  }
};
