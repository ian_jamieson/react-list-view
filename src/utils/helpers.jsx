hasValidFieldKeys: function(fields, data) {
    var invalidFields = fields.filter(function(field) {
        var hasKey = field.fieldKey in data;

        if(!hasKey) {
            console.error(field.fieldKey + " doesn't exist. Please make sure you match your 'fieldKey' to an existing column in your data table");
        } else {
            console.info(field.fieldKey + ': ' + data[field.fieldKey] + ' type:' + field.fieldType);
        }

        return !hasKey;
    });

    return invalidFields.length > 0;
},